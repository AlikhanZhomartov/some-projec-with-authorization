package com.example.testproject.constant;

public class TaskStatusConstant {
    public static final long RENDERING_ID = 1;
    public static final long COMPLETED_ID = 2;
}

package com.example.testproject.dto.requstDto.User;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserAuthorizationDto {

    @NotBlank(message = "Email can not be empty!")
    private String email;

    @NotBlank(message = "Password can not be empty!")
    private String password;
}

package com.example.testproject.dto.requstDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TaskCreateDto {

    @NotBlank(message = "description can not be empty!")
    private String description;
}

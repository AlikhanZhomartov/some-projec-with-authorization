package com.example.testproject.dto.responseDto.createTask;

import lombok.Data;

@Data
public class StatusDtoResponse {

    private Long id;

    private String statusName;
}

package com.example.testproject.dto.responseDto.createTask;

import com.example.testproject.dto.responseDto.UserDtoResponse;
import lombok.Data;

@Data
public class TaskDtoResponse {

    private Long id;

    private UserDtoResponse user;

    private String description;

    private StatusDtoResponse status;
}

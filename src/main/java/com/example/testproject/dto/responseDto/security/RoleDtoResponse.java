package com.example.testproject.dto.responseDto.security;

import lombok.Data;

@Data
public class RoleDtoResponse {

    private Long id;

    private String roleName;
}

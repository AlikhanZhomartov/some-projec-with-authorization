package com.example.testproject.dto.responseDto;

import com.example.testproject.dto.responseDto.security.RoleDtoResponse;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class UserDtoResponse {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private RoleDtoResponse role;

    private LocalDateTime createdDate;
}

package com.example.testproject.mapper.createTask;

import com.example.testproject.dto.responseDto.createTask.TaskDtoResponse;
import com.example.testproject.mapper.UserMapper;
import com.example.testproject.model.createTask.Task;

public class TaskMapper {

    public static TaskDtoResponse taskToDto(Task task) {
        TaskDtoResponse taskDtoResponse = new TaskDtoResponse();
        taskDtoResponse.setId(task.getId());
        taskDtoResponse.setUser(UserMapper.userToDto(task.getUser()));
        taskDtoResponse.setDescription(task.getDescription());
        taskDtoResponse.setStatus(StatusMapper.statusToDto(task.getStatus()));
        return taskDtoResponse;
    }

}

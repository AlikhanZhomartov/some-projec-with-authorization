package com.example.testproject.mapper.createTask;

import com.example.testproject.dto.responseDto.createTask.StatusDtoResponse;
import com.example.testproject.model.createTask.Status;

public class StatusMapper {

    public static StatusDtoResponse statusToDto(Status status) {
        StatusDtoResponse statusDtoResponse = new StatusDtoResponse();
        statusDtoResponse.setId(status.getId());
        statusDtoResponse.setStatusName(status.getStatusName());
        return statusDtoResponse;
    }
}

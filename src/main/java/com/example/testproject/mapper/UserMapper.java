package com.example.testproject.mapper;

import com.example.testproject.dto.responseDto.UserDtoResponse;
import com.example.testproject.mapper.security.RoleMapper;
import com.example.testproject.model.User;

public class UserMapper {

    public static UserDtoResponse userToDto(User user) {
        UserDtoResponse userDtoResponse = new UserDtoResponse();
        userDtoResponse.setId(user.getId());
        userDtoResponse.setName(user.getName());
        userDtoResponse.setSurname(user.getSurname());
        userDtoResponse.setEmail(user.getEmail());
        userDtoResponse.setRole(RoleMapper.roleToDto(user.getRole()));
        return userDtoResponse;
    }
}

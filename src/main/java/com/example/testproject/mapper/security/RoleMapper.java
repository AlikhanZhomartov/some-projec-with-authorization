package com.example.testproject.mapper.security;

import com.example.testproject.dto.responseDto.security.RoleDtoResponse;
import com.example.testproject.model.security.Role;

public class RoleMapper {

    public static RoleDtoResponse roleToDto(Role role) {
        RoleDtoResponse roleDtoResponse = new RoleDtoResponse();
        roleDtoResponse.setId(role.getId());
        roleDtoResponse.setRoleName(role.getRoleName());
        return roleDtoResponse;
    }
}

package com.example.testproject.service.serviceImpl.createTask;

import com.example.testproject.constant.TaskStatusConstant;
import com.example.testproject.dto.requstDto.TaskCreateDto;
import com.example.testproject.dto.responseDto.createTask.TaskDtoResponse;
import com.example.testproject.exception.domain.CustomNotFoundException;
import com.example.testproject.mapper.createTask.TaskMapper;
import com.example.testproject.model.createTask.Task;
import com.example.testproject.repository.createTask.TaskRepository;
import com.example.testproject.service.serviceInterface.UserService;
import com.example.testproject.service.serviceInterface.createTask.StatusService;
import com.example.testproject.service.serviceInterface.createTask.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.testproject.exception.ExceptionDescription.CustomNotFoundExceptionDescription;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final StatusService statusService;
    private final UserService userService;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, StatusService statusService, UserService userService) {
        this.taskRepository = taskRepository;
        this.statusService = statusService;
        this.userService = userService;
    }

    @Override
    public Task findById(Long taskId) {
        return taskRepository
                .findById(taskId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(CustomNotFoundExceptionDescription, "Task", "id", taskId)));
    }

    public List<Task> findAllByClientEmail(String email) {
        return taskRepository.findByUserEmail(email);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAllByStatus(Long statusId) {
        return taskRepository.findByStatusId(statusId);
    }

    @Override
    public List<Task> findAllByStatusAndClientEmail(Long statusId, String email) {
        return taskRepository.findByStatusIdAndUserEmail(statusId, email);
    }

    @Override
    public TaskDtoResponse findByIdDto(Long taskId) {
        return TaskMapper.taskToDto(findById(taskId));
    }

    @Override
    public List<TaskDtoResponse> findAllByClientEmailDto(String email) {
        return findAllByClientEmail(email).stream().map(TaskMapper::taskToDto).collect(Collectors.toList());
    }

    @Override
    public List<TaskDtoResponse> findAllDto() {
        return findAll().stream().map(TaskMapper::taskToDto).collect(Collectors.toList());
    }

    @Override
    public List<TaskDtoResponse> findAllByStatusDto(Long statusId) {
        return findAllByStatus(statusId).stream().map(TaskMapper::taskToDto).collect(Collectors.toList());
    }

    @Override
    public List<TaskDtoResponse> findAllByStatusAndClientEmailDto(Long statusId, String email) {
        return findAllByStatusAndClientEmail(statusId, email).stream().map(TaskMapper::taskToDto).collect(Collectors.toList());
    }

    @Override
    public Task save(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public void createTask(TaskCreateDto taskCreateDto, String clientEmail) {
        Task task = new Task();
        task.setUser(userService.findByEmail(clientEmail));
        task.setDescription(taskCreateDto.getDescription());
        task.setStatus(statusService.findById(TaskStatusConstant.RENDERING_ID));
        save(task);
    }

    @Override
    public void updateTaskStatus(Long taskId, Long statusId) {
        Task task = findById(taskId);
        task.setStatus(statusService.findById(statusId));
        save(task);
    }


}

package com.example.testproject.service.serviceImpl.security;

import com.example.testproject.exception.ExceptionDescription;
import com.example.testproject.exception.domain.CustomNotFoundException;
import com.example.testproject.model.security.Role;
import com.example.testproject.repository.security.RoleRepository;
import com.example.testproject.service.serviceInterface.security.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findByRoleName(String roleName) {
        return roleRepository.findByRoleName(roleName)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundExceptionDescription, "Role", "role name", roleName)));
    }
}

package com.example.testproject.service.serviceImpl;

import static com.example.testproject.constant.RoleNameConstant.*;
import com.example.testproject.constant.SecurityConstant;
import com.example.testproject.dto.requstDto.User.UserAuthorizationDto;
import com.example.testproject.dto.requstDto.User.UserRegistrationDto;
import com.example.testproject.dto.responseDto.UserDtoResponse;
import com.example.testproject.exception.domain.EmailExistException;
import com.example.testproject.mapper.UserMapper;
import com.example.testproject.model.User;
import com.example.testproject.repository.UserRepository;
import com.example.testproject.security.JWTTokenProvider;
import com.example.testproject.security.UserPrincipal;
import com.example.testproject.service.serviceInterface.UserService;
import com.example.testproject.service.serviceInterface.security.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static com.example.testproject.exception.ExceptionDescription.CustomNotFoundExceptionDescription;
import static com.example.testproject.exception.ExceptionDescription.EmailExistExceptionDescription;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JWTTokenProvider jwtTokenProvider;
    private final BCryptPasswordEncoder encoder;
    private final RoleService roleService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, @Lazy AuthenticationManager authenticationManager,
                           JWTTokenProvider jwtTokenProvider, BCryptPasswordEncoder encoder, RoleService roleService) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.encoder = encoder;
        this.roleService = roleService;
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = findByEmail(email);
        return new UserPrincipal(user);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException
                        (String.format(CustomNotFoundExceptionDescription, "User", "email", email)));
    }

    @Override
    public void emailExist(String email) {
        Optional<User> user = userRepository.findByEmail(email);
        if (user.isPresent()) throw new EmailExistException(String.format(EmailExistExceptionDescription, email));
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public UserDtoResponse registration(UserRegistrationDto userRegistrationDto) {

        emailExist(userRegistrationDto.getEmail().toLowerCase());

        User user = new User();
        user.setName(userRegistrationDto.getName());
        user.setSurname(userRegistrationDto.getSurname());
        user.setEmail(userRegistrationDto.getEmail().toLowerCase());
        user.setPassword(encoder.encode(userRegistrationDto.getPassword()));
        user.setRole(roleService.findByRoleName(CLIENT_ROLE));
        return UserMapper.userToDto(save(user));
    }

    @Override
    public ResponseEntity<UserDtoResponse> authorization(UserAuthorizationDto userAuthorizationDto, HttpServletRequest request) {

        String email = userAuthorizationDto.getEmail().toLowerCase().trim();
        String password = userAuthorizationDto.getPassword().trim();

        authenticate(email, password);

        User user = findByEmail(email);
        UserPrincipal userPrincipal = new UserPrincipal(user);
        String ipFromClient = jwtTokenProvider.getIpFromClient(request);
        HttpHeaders jwt = getJwtHeader(userPrincipal, ipFromClient);
        UserDtoResponse userDtoResponse = UserMapper.userToDto(user);
        return new ResponseEntity<>(userDtoResponse, jwt, HttpStatus.OK);
    }

    private HttpHeaders getJwtHeader(UserPrincipal userPrincipal, String ipFromClient) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(SecurityConstant.JWT_TOKEN_HEADER, jwtTokenProvider.generateToken(userPrincipal, ipFromClient));
        return httpHeaders;
    }

    private void authenticate(String email, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
    }
}

package com.example.testproject.service.serviceImpl.createTask;

import com.example.testproject.exception.domain.CustomNotFoundException;
import com.example.testproject.model.createTask.Status;
import com.example.testproject.repository.createTask.StatusRepository;
import com.example.testproject.service.serviceInterface.createTask.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.example.testproject.exception.ExceptionDescription.CustomNotFoundExceptionDescription;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public Status findById(Long statusId) {
        return statusRepository
                .findById(statusId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(CustomNotFoundExceptionDescription, "Status", "id", statusId)));
    }
}

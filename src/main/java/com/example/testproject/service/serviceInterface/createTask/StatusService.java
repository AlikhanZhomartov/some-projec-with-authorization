package com.example.testproject.service.serviceInterface.createTask;

import com.example.testproject.model.createTask.Status;

public interface StatusService {
    Status findById(Long statusId);
}

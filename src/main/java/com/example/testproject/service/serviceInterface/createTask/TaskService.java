package com.example.testproject.service.serviceInterface.createTask;

import com.example.testproject.dto.requstDto.TaskCreateDto;
import com.example.testproject.dto.responseDto.createTask.TaskDtoResponse;
import com.example.testproject.model.createTask.Task;

import java.util.List;

public interface TaskService {

    Task findById(Long taskId);

    List<Task> findAllByClientEmail(String email);

    List<Task> findAll();

    List<Task> findAllByStatus(Long statusId);

    List<Task> findAllByStatusAndClientEmail(Long statusId, String email);

    TaskDtoResponse findByIdDto(Long taskId);

    List<TaskDtoResponse> findAllByClientEmailDto(String email);

    List<TaskDtoResponse> findAllDto();

    List<TaskDtoResponse> findAllByStatusDto(Long statusId);

    List<TaskDtoResponse> findAllByStatusAndClientEmailDto(Long statusId, String email);

    Task save(Task task);

    void createTask(TaskCreateDto taskCreateDto, String clientEmail);

    void updateTaskStatus(Long taskId, Long statusId);
}

package com.example.testproject.service.serviceInterface.security;

import com.example.testproject.model.security.Role;

public interface RoleService {

    Role findByRoleName(String roleName);
}

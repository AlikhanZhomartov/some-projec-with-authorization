package com.example.testproject.service.serviceInterface;

import com.example.testproject.dto.requstDto.User.UserAuthorizationDto;
import com.example.testproject.dto.requstDto.User.UserRegistrationDto;
import com.example.testproject.dto.responseDto.UserDtoResponse;
import com.example.testproject.model.User;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface UserService {

    User findByEmail(String email);

    void emailExist(String email);

    User save(User user);

    UserDtoResponse registration(UserRegistrationDto userRegistrationDto);

    ResponseEntity<UserDtoResponse> authorization(UserAuthorizationDto userAuthorizationDto, HttpServletRequest request);
}

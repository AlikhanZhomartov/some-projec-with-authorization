package com.example.testproject.repository.createTask;

import com.example.testproject.model.createTask.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByUserEmail(String email);
    List<Task> findByStatusId(Long id);
    List<Task> findByStatusIdAndUserEmail(Long id, String email);
}

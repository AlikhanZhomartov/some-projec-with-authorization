package com.example.testproject.controller;

import com.example.testproject.dto.requstDto.User.UserAuthorizationDto;
import com.example.testproject.dto.requstDto.User.UserRegistrationDto;
import com.example.testproject.dto.responseDto.UserDtoResponse;
import com.example.testproject.exception.ExceptionHandling;
import com.example.testproject.service.serviceInterface.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController extends ExceptionHandling {

    private final UserService userService;

    @Autowired
    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registration")
    public ResponseEntity<UserDtoResponse> registration(@Valid UserRegistrationDto userRegistrationDto) {
        UserDtoResponse userDtoResponse = userService.registration(userRegistrationDto);
        return new ResponseEntity<>(userDtoResponse, HttpStatus.CREATED);
    }

    @PostMapping("/authorization")
    public ResponseEntity<UserDtoResponse> authorization(@Valid UserAuthorizationDto userAuthorizationDto, HttpServletRequest request) {
       return userService.authorization(userAuthorizationDto, request);
    }
}

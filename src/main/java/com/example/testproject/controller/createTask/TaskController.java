package com.example.testproject.controller.createTask;

import com.example.testproject.dto.requstDto.TaskCreateDto;
import com.example.testproject.dto.responseDto.createTask.TaskDtoResponse;
import com.example.testproject.exception.ExceptionHandling;
import com.example.testproject.service.serviceInterface.createTask.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/v1/task")
public class TaskController extends ExceptionHandling {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/create-task")
    public ResponseEntity<HttpStatus> createTask(@Valid TaskCreateDto taskCreateDto, Principal principal) {
        taskService.createTask(taskCreateDto, principal.getName());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/update-task-status")
    public ResponseEntity<HttpStatus> updateTaskStatus(@RequestParam(name = "task_id") Long taskId, @RequestParam(name = "status_id") Long statusId) {
        taskService.updateTaskStatus(taskId, statusId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/get-by-task-id")
    public ResponseEntity<TaskDtoResponse> getTaskById(@RequestParam(name = "task_id") Long taskId) {
        return new ResponseEntity<>(taskService.findByIdDto(taskId), HttpStatus.OK);
    }

    @GetMapping("/get-all-task")
    public ResponseEntity<List<TaskDtoResponse>> getAllTask() {
        return new ResponseEntity<>(taskService.findAllDto(), HttpStatus.OK);
    }

    @GetMapping("/get-all-by-client-email")
    public ResponseEntity<List<TaskDtoResponse>> getAllByClientEmail(@RequestParam(name = "email") String email) {
        return new ResponseEntity<>(taskService.findAllByClientEmailDto(email), HttpStatus.OK);
    }

    @GetMapping("/get-all-by-status")
    public ResponseEntity<List<TaskDtoResponse>> getAllByStatus(@RequestParam(name = "status_id") Long statusId) {
        return new ResponseEntity<>(taskService.findAllByStatusDto(statusId), HttpStatus.OK);
    }

    @GetMapping("/get-all-by-status-and-client-email")
    public ResponseEntity<List<TaskDtoResponse>> getAllByStatusAndClientEmail(@RequestParam(name = "status_id") Long statusId, @RequestParam(name = "email") String email) {
        return new ResponseEntity<>(taskService.findAllByStatusAndClientEmailDto(statusId, email), HttpStatus.OK);
    }
}

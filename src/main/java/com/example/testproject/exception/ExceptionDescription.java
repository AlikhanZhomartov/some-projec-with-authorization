package com.example.testproject.exception;

public class ExceptionDescription {
    public static final String CustomNotFoundExceptionDescription = "%1$s not found by %2$s: %3$s";
    public static final String EmailExistExceptionDescription = "%1$s is already exist";
}

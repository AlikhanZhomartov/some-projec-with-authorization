package com.example.testproject.model.createTask;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "statuses")
@Data
public class Status {

    @Id
    private Long id;

    @Column(nullable = false, unique = true)
    private String statusName;
}
